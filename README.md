*Make a sample Gitlab CI pipeline to run tests every commit for a basic node js project*

Make a sample app - using this as a guide https://expressjs.com/en/starter/generator.html - but basically one command `npx express-generator` (attached tar.gz is that output)


Commit it to a fresh git project and push to gitlab.com as a new project


Make new project public


Then make a .gitlab-ci.yml file to make a new pipeline.
- test stage must run `npm test` and fail if tests fail (add a test stage with the line ` "test":"echo 'hello world'"` to the package.json scripts section if absent).
- archive stage must npm pack and allow the tar.gz result to be downloaded as an artifact.

It will require understanding gitlab-ci, the yaml file syntax, docker images and git.

A gzipped tar file, test-app2.tar.gz, has been attached in case of difficultly running node on the local box.

